
### Python flask app

- python flask 🐳 ✅
- (db) Redis 🐳


### Run?

```bash
docker network create flask-redis

docker run -p 8000:5000 --network=flask-redis -it --env "FOO=docker run" python-flask

docker run -d --network=flask-redis --name redis redis
```

### Exercise Docker-compose

Write a docker-compose.yaml file that:

- has a service named `redis`
- has a service called `web`
- builds the app with `build: .`
- exposes the `web` app on port 8000
- injects and receives an environment variable `MESSAGE`

- run your stack with `docker-compose up --build`
