# [sre-bootcamp](https://kontinu.gitlab.io/sre-bootcamp)

> 2022 bootcamp


# Todo

- [x] improve app to do a central dashboard of "pods", communication via Redis and TTL in keys.

https://realpython.com/python-redis/#using-key-expiry
https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Auto-placement_in_CSS_Grid_Layout

- [ ] Atlantis with terraform https://www.runatlantis.io/guide/testing-locally.html#install-terraform
